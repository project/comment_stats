--------------
Comment Stats
--------------

-----------
DESCRIPTION
-----------

This module provides a deeper view to comment analysis. This module divides the
discussion on the content into two categories.
 A. Comments
 B. Replies

The threads which are not replies are called Comments.
Replies are those threads which are responses to the comments.

For Example: In facebook, 'comments' are the threads which comes under post and
replies are the sub threads of comments.

Comments stats provides a tabular view to users, comments, replies & unapproved
comments.

-----------------------
Drupal required version
-----------------------
Drupal 7.x

----------
INSTALLING
----------
1. Copy the 'comment_stats' folder to your sites/all/modules directory.

2. Go to admin/modules and enable the module.
Refer instalation modules at http://drupal.org/node/70151

-------------
CONFIGURATION
-------------
Go to admin/content/commentstats/config. Follow instructions on this page.
